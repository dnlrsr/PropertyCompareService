
[![pipeline status](https://gitlab.com/Dinodanio/PropertyCompareService/badges/master/pipeline.svg)](https://gitlab.com/Dinodanio/PropertyCompareService/commits/master)
[![coverage report](https://gitlab.com/Dinodanio/PropertyCompareService/badges/master/coverage.svg)](https://gitlab.com/Dinodanio/PropertyCompareService/commits/master)

# Endpoints

## PropertyCompare Endpoints
```
/api/v1/property-compare (POST) 
```
