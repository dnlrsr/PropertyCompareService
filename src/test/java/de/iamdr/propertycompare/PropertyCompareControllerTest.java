package de.iamdr.propertycompare;

import de.iamdr.Application;
import org.apache.commons.lang3.Validate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
@AutoConfigureMockMvc
public class PropertyCompareControllerTest {
  @Autowired
  private MockMvc mockMvc;

  @Test
  public void givenRequestHasBeenMade_whenPostToApplication_thenResponseOK() throws Exception {
    final ClassLoader classLoader = getClass().getClassLoader();
    final File referenceFile = new File(Validate.notNull(classLoader.getResource("reference.properties")).getFile());
    final File candidateFile = new File(Validate.notNull(classLoader.getResource("candidate.properties")).getFile());

    MockMultipartFile referenceMultipartFile =
            new MockMultipartFile(
                    "referenceFile",
                    "reference.properties",
                    MediaType.TEXT_PLAIN_VALUE,
                    Files.readAllBytes(Paths.get(referenceFile.getAbsolutePath()))
            );

    MockMultipartFile candidateMultipartFile =
            new MockMultipartFile(
                    "candidateFile",
                    "candidate.properties",
                    MediaType.TEXT_PLAIN_VALUE,
                    Files.readAllBytes(Paths.get(candidateFile.getAbsolutePath()))
            );

    this.mockMvc.perform(
            multipart("/api/v1/property-compare")
                    .file(referenceMultipartFile)
                    .file(candidateMultipartFile)
                    .contentType(MediaType.MULTIPART_FORM_DATA_VALUE))
            .andDo(MockMvcResultHandlers.log())
            .andExpect(status().isOk())
            .andExpect(header().stringValues("Content-Type", MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("{\"onlyInCandidate\":[],\"diffValues\":[{\"property1\":{\"left\":\"false\",\"right\":\"true\"}}],\"onlyInReference\":[{\"property2\":\"false\"}]}"));

  }
}
