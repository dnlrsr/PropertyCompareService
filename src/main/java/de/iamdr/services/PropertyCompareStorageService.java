package de.iamdr.services;

import org.apache.commons.lang3.Validate;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.stream.Stream;

// Todo: Implement this shit
@Repository
public class PropertyCompareStorageService {

  public void init() {

  }

  public String store(MultipartFile file) throws IOException {
    final File tempFile = File.createTempFile(Validate.notNull(file.getOriginalFilename()), "");
    file.transferTo(tempFile);

    return tempFile.getAbsolutePath();
  }

  Stream<Path> loadAll() {
    return null;
  }

  Path load(String filename) {
    return null;
  }

  Resource loadAsResource(String filename) {
    return null;
  }

  public void deleteAll() {

  }
}
