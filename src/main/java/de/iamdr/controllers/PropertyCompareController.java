package de.iamdr.controllers;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.MapDifference;
import de.iamdr.services.PropertyCompareStorageService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static de.iamdr.utils.PropertyCompareUtils.compareProperties;

@RequestMapping("api/v1/property-compare")
@RestController
public class PropertyCompareController {

  private final static Logger LOGGER = LogManager.getLogger(PropertyCompareController.class);

  private final PropertyCompareStorageService storageService;

  @Autowired
  public PropertyCompareController(PropertyCompareStorageService storageService) {
    this.storageService = storageService;
  }

  /**
   * This endpoint returns a diff with the google {@link MapDifference}.
   * <br/><br/>
   * <span style="color: red;">Conditions: </span>
   * <br/>
   * <ul>
   *   <li>The request must be a multipart request.</li>
   *   <li>Both files must be given.</li>
   * </ul>
   *
   * @param candidateFile to diff.
   * @param referenceFIle to diff.
   * @return an object with the diff result.
   */
  @PostMapping
  public ResponseEntity compareFiles(@RequestParam("referenceFile") MultipartFile candidateFile,
                                     @RequestParam("candidateFile") MultipartFile referenceFIle) {

    if (candidateFile.isEmpty() || referenceFIle.isEmpty()) {
      return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body("No file!");
    }

    try {
      final String referencePath = this.storageService.store(referenceFIle);
      final String candidatePath = this.storageService.store(candidateFile);

      return ResponseEntity
              .status(HttpStatus.OK)
              .contentType(MediaType.APPLICATION_JSON)
              .body(new ObjectMapper()
                      .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
                      .writeValueAsString(compareProperties(candidatePath, referencePath)));
    } catch (final IOException ioe) {
      if (LOGGER.isErrorEnabled()) {
        LOGGER.error(ioe);
      }
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ioe.getMessage());
    }
  }
}

